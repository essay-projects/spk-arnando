<?php $email = $this->model_app->email($this->session->userdata('user')); ?>

<section class="content-header">
  <h1>
    KEPUTUSAN WP
  </h1>
</section>

<section class="content">

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Tabel Pencocokan Kriteria</h3>
      </div>
      <div class="box-body">
        <table class="table table-hover table-bordered data-tables">
          <thead>
            <tr>
              <th>#</th>
              <th>Alternatif</th>
              <th>Nama</th>
              <?php foreach ($kriteria as $key): ?>
                <th><?= $key['id'] ?></th>
              <?php endforeach ?>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($alternatif==NULL): ?>
              <tr>
                <td class="text-center" colspan="<?= 2+count($kriteria)  ?>">Silahkan lengkapi data di halaman <a href="<?= base_url('app') ?>" class="text-black"><u>Data.</u></a></td>
              </tr>
            <?php endif ?>

            <?php $number = 1; foreach ($alternatif as $keys): ?>
              <tr>
                <td><?= $number++ ?>.</td>
                <td><?= $keys['id'] ?></td>
                <td><?= $keys["nama"] ?></td>
                <?php foreach ($kriteria as $key): ?>
                  <td>
                    <?php 
                      $data_pencocokan = $this->model_app->data_pencocokan_kriteria($email,$keys['id'],$key['id']);
                      echo $data_pencocokan['nilai'];
                    ?>
                  </td>
                <?php endforeach ?>

                <?php $cek_tombol = $this->model_app->untuk_tombol($email,$keys['id']); ?>

                <td width="7%">
                  <?php if ($cek_tombol==0) { ?>
                    <a href="#set<?= $keys['id'] ?>" data-toggle="modal" class="btn btn-primary btn-sm" title="edit"><i class="fa fa-pencil"></i> Set</a>
                  <?php } else { ?>
                    <a href="#edit<?= $keys['id'] ?>" data-toggle="modal" class="btn btn-primary btn-sm" title="edit"><i class="fa fa-pencil"></i> Edit</a>
                  <?php } ?>
                </td>
              </tr>
              
              <div class="modal fade" id="set<?= $keys['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Pencocokan</h4>
                          </div>
                          <?= form_open('app/pencocokankriteria') ?>
                              <div class="modal-body">
                                  <?php foreach ($kriteria as $key): ?>
                                    <?php 
                                      $sub_kriteria = $this->model_app->data_sub_kriteria($key['id'],$email);
                                    ?>
                                    <?php if ($sub_kriteria!=NULL): ?>
                                      <input type="text" name="a" value="<?= $keys['id'] ?>" hidden>
                                      <input type="text" name="c[]" value="<?= $key['id'] ?>" hidden>
                                      <div class="form-group">
                                        <label for="<?= $key['id'] ?>"><?= $key['id'] ?></label>
                                        <select name="nilai[]" class="form-control" required>
                                          <option value="">--Pilih--</option>
                                          <?php foreach ($sub_kriteria as $subs_kriteria): ?>
                                            <option value="<?= $subs_kriteria['id'] ?>"><?= $subs_kriteria['deskripsi'] ?> (<?= $subs_kriteria['range'] ?>)</option>
                                          <?php endforeach ?>
                                        </select>
                                      </div>
                                    <?php endif ?>
                                  <?php endforeach ?>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>

              <div class="modal fade" id="edit<?= $keys['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Pencocokan</h4>
                          </div>
                          <?= form_open('app/editpencocokankriteria') ?>
                              <div class="modal-body">
                                  <?php foreach ($kriteria as $key): ?>
                                    <?php 
                                      $sub_kriteria = $this->model_app->data_sub_kriteria($key['id'],$email);
                                    ?>
                                    <?php if ($sub_kriteria!=NULL): ?>
                                      <input type="text" name="a" value="<?= $keys['id'] ?>" hidden>
                                      <input type="text" name="c[]" value="<?= $key['id'] ?>" hidden>
                                      <div class="form-group">
                                        <label for="<?= $key['id'] ?>"><?= $key['id'] ?></label>
                                        <select name="nilai[]" class="form-control" required>
                                          <option value="">--Pilih--</option>
                                          <?php foreach ($sub_kriteria as $subs_kriteria): ?>
                                            <?php 
                                              $s_option = $this->model_app->untuk_option($email,$keys['id'],$subs_kriteria['kriteria']);
                                            ?>
                                            <option value="<?= $subs_kriteria['id'] ?>" <?php if($subs_kriteria['id']==$s_option['id_nilai']){echo "selected";} ?>><?= $subs_kriteria['deskripsi'] ?> (<?= $subs_kriteria['range'] ?>)</option>
                                          <?php endforeach ?>
                                        </select>
                                      </div>
                                    <?php endif ?>
                                  <?php endforeach ?>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                  <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php
  $j = 0;
  foreach ($alternatif as $keys){ 
    $s = 1;
    foreach ($kriteria as $key) {
      $pangkat = $this->model_app->pangkat($email,$key['id']);
      if ($pangkat['jenis']=='Cost') {
        $p = -$pangkat['nilai'];
      } else {
        $p = +$pangkat['nilai'];
      }

      $c = $this->model_app->data_pencocokan_kriteria($email,$keys['id'],$key['id']);
      $s *= $c['nilai']**$p;
    }
    $j += $s;
  }
?>

<?php if ($convert==NULL or $alternatif==NULL or $cek_pencocokan==NULL or $j==0) { ?>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-body text-center" style="height: 200px; padding-top: 90px;">
          isi tabel di atas
        </div>
      </div>
    </div>
  </div>
<?php } else { ?>

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Convert W</h3>
      </div>
      <div class="box-body">
        <table class="table table-hover table-bordered">
          <thead>
            <tr>
              <th>Kriteria</th>
              <th>Nilai</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($convert as $key): ?>
              <tr>
                <td><?= $key['id'] ?></td>
                <td>
                  <?php 
                    if ($key['jenis']=='Benefit') {
                      $criteriaWeights[$key['id']] = +$key['nilai'];
                      echo +$key['nilai'];
                    } else {
                      $criteriaWeights[$key['id']] = -$key['nilai'];
                      echo -$key['nilai'];
                    }
                  ?>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Nilai S</h3>
      </div>
      <div class="box-body">
        <table class="table table-hover table-bordered data-tables">
          <thead>
            <tr>
              <th>#</th>
              <th>Alternatif</th>
              <th>Nama</th>
              <?php foreach ($kriteria as $key): ?>
                <th><?= $key['id'] ?></th>
              <?php endforeach ?>
              <th>Nilai S</th>
            </tr>
          </thead>
          <tbody>
            <?php $number = 1; foreach ($alternatif as $keys): ?>
              <tr>
                <td><?= $number++ ?>.</td>
                <td>
                  <?php
                    preg_match_all("!\d+!", $keys["id"], $matches);
                    echo "S". implode("", $matches[0]);
                  ?>
                </td>
                <td><?= $keys["nama"] ?></td>
                <?php 
                  $s = 1;
                  foreach ($kriteria as $key) {
                    $pangkat = $this->model_app->pangkat($email,$key['id']);
                    if ($pangkat['jenis']=='Cost') {
                      $p = -$pangkat['nilai'];
                    } else {
                      $p = +$pangkat['nilai'];
                    }

                    $c = $this->model_app->data_pencocokan_kriteria($email,$keys['id'],$key['id']);
                    $per = $c['nilai']**$p;
                    $s *= $per;
                    echo "<td>".number_format($per, 4, '.', '')."</td>";
                  }
                  echo "<td>".number_format($s, 4, '.', '')."</td>";
                ?>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Nilai V</h3>
      </div>
      <div class="box-body">
        <table class="table table-hover table-bordered data-tables">
          <thead>
            <tr>
              <th>#</th>
              <th>Alternatif</th>
              <th>Nama</th>
              <th>Nilai V</th>
            </tr>
          </thead>
          <tbody>
            <?php $number = 1; foreach ($alternatif as $keys): ?>
              <tr>
                <td><?= $number++ ?>.</td>
                <td>
                  <?php
                    preg_match_all("!\d+!", $keys["id"], $matches);
                    echo "V". implode("", $matches[0]);
                  ?>
                </td>
                <td><?= $keys["nama"] ?></td>
                <td>
                  <?php 
                    $s = 1;
                    foreach ($kriteria as $key) {
                      $pangkat = $this->model_app->pangkat($email, $key['id']);
                      if ($pangkat['jenis']=='Cost') {
                        $p = -$pangkat['nilai'];
                      } else {
                        $p = +$pangkat['nilai'];
                      }

                      $c = $this->model_app->data_pencocokan_kriteria($email,$keys['id'],$key['id']);
                      $s *= $c['nilai']**$p;
                    }
                    $wpAmounts[$keys["id"]] = $s/$j;

                    echo number_format($wpAmounts[$keys["id"]], 4, ".", "");
                  ?>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  
  <div class="col-md-6">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Perangkingan</h3>
      </div>
      <div class="box-body">
        <table class="table table-hover table-bordered data-tables-ranking">
          <thead>
            <tr>
              <th hidden>#</th>
              <th>Alternatif</th>
              <th>Nama</th>
              <th>Nilai V</th>
            </tr>
          </thead>
          <tbody>
            <?php $number = 1; foreach ($alternatif as $keys): ?>
              <tr>
                <td hidden><?= $number++ ?>.</td>
                <td>
                  <?php
                    preg_match_all("!\d+!", $keys["id"], $matches);
                    echo "V". implode("", $matches[0]);
                  ?>
                </td>
                <td><?= $keys["nama"] ?></td>
                <td>
                  <?php 
                    $s = 1;
                    foreach ($kriteria as $key) {
                      $pangkat = $this->model_app->pangkat($email, $key['id']);
                      if ($pangkat['jenis']=='Cost') {
                        $p = -$pangkat['nilai'];
                      } else {
                        $p = +$pangkat['nilai'];
                      }

                      $c = $this->model_app->data_pencocokan_kriteria($email,$keys['id'],$key['id']);
                      $s *= $c['nilai']**$p;
                    }
                    $wpAmounts[$keys["id"]] = $s/$j;

                    echo number_format($wpAmounts[$keys["id"]], 4, ".", "");

                    if ($wpAmounts[$keys["id"]] > @$maxWP["nilai"]) {
                      $maxWP = [
                        "id" => $keys["id"],
                        "nama" => $keys["nama"],
                        "nilai" => $wpAmounts[$keys["id"]]
                      ];
                    }
                  ?>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Hasil Keputusan WP</h3>
      </div>
      <div class="box-body">
        Hasil perhitungan menggunakan metode WP. Alternatif terbaik adalah <b><?= $maxWP["id"] ?></b> yaitu <b><?= $maxWP["nama"] ?></b> dengan total nilai V = <b><?= number_format($maxWP["nilai"], 4, ".", "") ?>.</b>
      </div>
    </div>
  </div>
</div>

<?php } ?>
</section>

<section class="content-header">
  <h1>
    KEPUTUSAN SAW
  </h1>
</section>

<section class="content">

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Matriks Keputusan</h3>
      </div>
      <div class="box-body">
        <table class="data-tables table table-hover table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Alternatif</th>
              <th>Name</th>
              <?php foreach ($kriteria as $key): ?>
                <th><?= $key['id'] ?></th>
              <?php endforeach ?>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($alternatif==NULL): ?>
              <tr>
                <td class="text-center" colspan="<?= 2+count($kriteria)  ?>">Silahkan lengkapi data di halaman <a href="<?= base_url('app') ?>" class="text-black"><u>Data.</u></a></td>
              </tr>
            <?php endif ?>

            <?php $number = 1; foreach ($alternatif as $keys): ?>
              <tr>
                <td><?= $number++ ?>.</td>
                <td><?= $keys['id'] ?></td>
                <td><?= $keys['nama'] ?></td>
                <?php foreach ($kriteria as $key): ?>
                  <td>
                    <?php 
                      $data_pencocokan = $this->model_app->data_pencocokan_kriteria($email, $keys['id'], $key['id']);
                      echo $data_pencocokan['nilai'];

                      $sawBK[$keys['id']][$data_pencocokan['c']]  = $data_pencocokan['nilai'];
                      $sawKB[$data_pencocokan['c']][$keys['id']]  = $data_pencocokan['nilai'];
                    ?>
                  </td>
                <?php endforeach ?>

                <?php $cek_tombol = $this->model_app->untuk_tombol($email,$keys['id']); ?>

                <td width="7%">
                  <?php if ($cek_tombol==0) { ?>
                    <a href="#set<?= $keys['id'] ?>" data-toggle="modal" class="btn btn-primary btn-sm" title="edit"><i class="fa fa-pencil"></i> Set</a>
                  <?php } else { ?>
                    <a href="#edit<?= $keys['id'] ?>" data-toggle="modal" class="btn btn-primary btn-sm" title="edit"><i class="fa fa-pencil"></i> Edit</a>
                  <?php } ?>
                </td>
              </tr>
              
              <div class="modal fade" id="set<?= $keys['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Pencocokan</h4>
                          </div>
                          <?= form_open('app/pencocokankriteria') ?>
                              <div class="modal-body">
                                  <?php foreach ($kriteria as $key): ?>
                                    <?php 
                                      $sub_kriteria = $this->model_app->data_sub_kriteria($key['id'],$email);
                                    ?>
                                    <?php if ($sub_kriteria!=NULL): ?>
                                      <input type="text" name="a" value="<?= $keys['id'] ?>" hidden>
                                      <input type="text" name="c[]" value="<?= $key['id'] ?>" hidden>
                                      <div class="form-group">
                                        <label for="<?= $key['id'] ?>"><?= $key['id'] ?></label>
                                        <select name="nilai[]" class="form-control" required>
                                          <option value="">--Pilih--</option>
                                          <?php foreach ($sub_kriteria as $subs_kriteria): ?>
                                            <option value="<?= $subs_kriteria['id'] ?>"><?= $subs_kriteria['deskripsi'] ?> (<?= $subs_kriteria['range'] ?>)</option>
                                          <?php endforeach ?>
                                        </select>
                                      </div>
                                    <?php endif ?>
                                  <?php endforeach ?>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>

              <div class="modal fade" id="edit<?= $keys['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Pencocokan</h4>
                          </div>
                          <?= form_open('app/editpencocokankriteria') ?>
                              <div class="modal-body">
                                  <?php foreach ($kriteria as $key): ?>
                                    <?php 
                                      $sub_kriteria = $this->model_app->data_sub_kriteria($key['id'],$email);
                                    ?>
                                    <?php if ($sub_kriteria!=NULL): ?>
                                      <input type="text" name="a" value="<?= $keys['id'] ?>" hidden>
                                      <input type="text" name="c[]" value="<?= $key['id'] ?>" hidden>
                                      <div class="form-group">
                                        <label for="<?= $key['id'] ?>"><?= $key['id'] ?></label>
                                        <select name="nilai[]" class="form-control" required>
                                          <option value="">--Pilih--</option>
                                          <?php foreach ($sub_kriteria as $subs_kriteria): ?>
                                            <?php 
                                              $s_option = $this->model_app->untuk_option($email,$keys['id'],$subs_kriteria['kriteria']);
                                            ?>
                                            <option value="<?= $subs_kriteria['id'] ?>" <?php if($subs_kriteria['id']==$s_option['id_nilai']){echo "selected";} ?>><?= $subs_kriteria['deskripsi'] ?> (<?= $subs_kriteria['range'] ?>)</option>
                                          <?php endforeach ?>
                                        </select>
                                      </div>
                                    <?php endif ?>
                                  <?php endforeach ?>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                  <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
            <?php endforeach ?>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="3" class="text-center">MIN</td>
              <?php foreach ($kriteria as $key): ?>
                <td><?= $sawMin[$key['id']] = min($sawKB[$key['id']]) ?></td>
              <?php endforeach ?>
              <td></td>
            </tr>
            <tr>
              <td colspan="3" class="text-center">MAX</td>
              <?php foreach ($kriteria as $key): ?>
                <td><?= $sawMax[$key['id']] = max($sawKB[$key['id']]) ?></td>
              <?php endforeach ?>
              <td></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Normalisasi</h3>
      </div>
      <div class="box-body">
        <table class="data-tables table table-hover table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Alternatif</th>
              <th>Name</th>
              <?php foreach ($kriteria as $key): ?>
                <th><?= $key['id'] ?></th>
              <?php endforeach ?>
            </tr>
          </thead>
          <tbody>
            <?php if ($alternatif==NULL): ?>
              <tr>
                <td class="text-center" colspan="<?= 2+count($kriteria)  ?>">Silahkan lengkapi data di halaman <a href="<?= base_url('app') ?>" class="text-black"><u>Data.</u></a></td>
              </tr>
            <?php endif ?>

            <?php $number = 1; foreach ($alternatif as $keys): ?>
              <tr>
                <td><?= $number++ ?>.</td>
                <td><?= $keys['id'] ?></td>
                <td><?= $keys['nama'] ?></td>
                <?php foreach ($kriteria as $key): ?>
                  <td>
                    <?php 
                      $data_pencocokan = $this->model_app->data_pencocokan_kriteria($email, $keys['id'], $key['id']);
                      $sawNormalitations[$keys["id"]][$key["id"]] = (float) $data_pencocokan['nilai']/$sawMax[$key['id']];
                      echo number_format($sawNormalitations[$keys["id"]][$key["id"]], 2, '.', '');
                    ?>
                  </td>
                <?php endforeach ?>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Pembobotan</h3>
      </div>
      <div class="box-body">
        <table class="table table-hover table-bordered data-tables">
          <thead>
            <tr>
              <th>#</th>
              <th>Alternatif</th>
              <th>Name</th>
              <?php foreach ($kriteria as $key): ?>
                <th><?= $key['id'] ?></th>
              <?php endforeach ?>
              <th>Jumlah</th>
            </tr>
          </thead>
          <tbody>
            <?php $number = 1; foreach ($alternatif as $keys): ?>
                <tr>
                <td><?= $number++ ?>.</td>
                <td><?= $keys['id'] ?></td>
                <td><?= $keys['nama'] ?></td>
                <?php foreach ($kriteria as $key): ?>
                  <td>
                    <?php
                      $sawNB[$keys["id"]][$key["id"]] = $sawNormalitations[$keys["id"]][$key["id"]]*$criteriaWeights[$key["id"]];
                      @$sawAmounts[$keys["id"]] += $sawNB[$keys["id"]][$key["id"]];
                      echo number_format($sawNB[$keys["id"]][$key["id"]], 4, '.', '');
                    ?>
                  </td>
                <?php endforeach ?>
                <td><?= number_format($sawAmounts[$keys["id"]], 4, '.', '') ?></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Perankingan</h3>
      </div>
      <div class="box-body">
        <table class="table table-hover table-bordered data-tables-ranking">
          <thead>
            <tr>
              <th hidden>#</th>
              <th>Alternatif</th>
              <th>Nama</th>
              <th>Jumlah</th>
            </tr>
          </thead>
          <tbody>
            <?php $number = 1; foreach ($alternatif as $keys): ?>
              <tr>
                <td hidden></td>
                <td><?= $keys['id'] ?></td>
                <td><?= $keys['nama'] ?></td>
                <td><?= number_format($sawAmounts[$keys["id"]], 4, '.', '') ?></td>
              </tr>
            <?php
              if ($sawAmounts[$keys["id"]] > @$maxSAW["nilai"]) {
                $maxSAW = [
                  "id" => $keys["id"],
                  "nama" => $keys['nama'],
                  "nilai" => $sawAmounts[$keys["id"]]
                ];
              }
              endforeach
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Hasil Keputusan SAW</h3>
      </div>
      <div class="box-body">
        Hasil perhitungan menggunakan metode SAW. Alternatif terbaik adalah <b><?= $maxSAW["id"] ?></b> yaitu <b><?= $maxSAW["nama"] ?></b> dengan total nilai V = <b><?= number_format($maxSAW["nilai"], 4, ".", "") ?>.</b>
      </div>
    </div>
  </div>
</div>

</section>