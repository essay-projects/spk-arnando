<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?= base_url() ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?= base_url() ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/logouin.png'); ?>" rel="icon" type="image">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <a href="<?= base_url('app') ?>" class="logo"><b>SPK </b> WP & SAW</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="<?= base_url('logout') ?>" class="dropdown-toggle">
                  <span class="hidden-xs">Logout</span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            
            <li class="<?php if($page=='data'){echo 'active';} ?>">
              <a href="<?= base_url('app') ?>">
                <i class="fa fa-th"></i> <span>Data</span>
              </a>
            </li>
            <li class="<?php if($page=='keputusan'){echo 'active';} ?>">
              <a href="<?= base_url('app/keputusan') ?>">
                <i class="fa fa-envelope"></i> <span>Keputusan</span>
              </a>
            </li>
            <li class="<?php if($page=='pengaturan'){echo 'active';} ?>">
              <a href="<?= base_url('app/pengaturan') ?>">
                <i class="fa fa-gear"></i> <span>Pengaturan</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
				<?php $this->load->view('app/content/content_'.$page); ?>
      </div>
      <!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>HMKK</b> OFFICIAL
        </div>
        <strong>Created By <a href="http://www.instagram.com/a.yusron" target="_blank">Arnando Firhan Prayudha</a>.</strong> Aplikasi SPK dengan Metode SAW & WP.
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script src="<?= base_url() ?>assets/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="<?= base_url() ?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?= base_url() ?>assets/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url() ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
      $(document).ready( function () {
          $(".data-tables").DataTable();

          var tableRanking = $(".data-tables-ranking")
            .dataTable({
                "order": [[3, "desc"], [1, "asc"]],
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                  }]
            });

            tableRanking.on("draw.dt", function () {
              var PageInfo = $(".data-tables-ranking").DataTable().page.info();
              tableRanking.column(0, {
                page: "current"
              }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
              });
            });
      });
    </script>
  </body>
</html>