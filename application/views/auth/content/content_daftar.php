<div class="p-t-30 p-b-50">
    <?= form_open('buatakun', 'class="login100-form validate-form p-b-33 p-t-5 m-b-22"') ?>
        <div class="wrap-input100 validate-input" data-validate = "Enter email">
            <input class="input100" type="email" name="email" placeholder="Email">
            <span class="focus-input100" data-placeholder="&#xe82a;"></span>
        </div>
        <div class="wrap-input100 validate-input" data-validate = "Enter username">
            <input class="input100" type="text" name="username" placeholder="Username">
            <span class="focus-input100" data-placeholder="&#xe82a;"></span>
        </div>

        <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="password" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xe80f;"></span>
        </div>

        <div class="wrap-input100 validate-input" data-validate="Enter password again.">
            <input class="input100" type="password" name="passwordlagi" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xe80f;"></span>
        </div>

        <div class="container-login100-form-btn m-t-32">
            <button class="login100-form-btn">
                Daftar
            </button>
        </div>
    </form>
    
    <a class="text-center" style="color: white;" href="<?= base_url() ?>">
        &#9754; Already membership? 
    </a>
</div>