<div class="wrap-login100 p-t-30 p-b-50">
    <span class="login100-form-title p-b-41">
        SPK Metode WP dan SAW
    </span>

    <?= form_open('ceklogin', 'class="login100-form validate-form p-b-33 p-t-5 m-b-22"') ?>
        <div class="wrap-input100 validate-input" data-validate = "Enter email">
            <input class="input100" type="text" name="email" placeholder="Email">
            <span class="focus-input100" data-placeholder="&#xe82a;"></span>
        </div>

        <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="password" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xe80f;"></span>
        </div>

        <div class="container-login100-form-btn m-t-32">
            <button class="login100-form-btn">
                Login
            </button>
        </div>
    </form>
    
    <a class="text-center" style="color: white;" href="<?= base_url('daftar') ?>">
        Register a new membership &#10140;
    </a>
</div>